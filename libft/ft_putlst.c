/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaize <mlaize@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 14:41:32 by mlaize            #+#    #+#             */
/*   Updated: 2015/03/17 10:55:24 by mlaize           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putlst(t_list *lst)
{
	int	i;

	i = 0;
	while (lst)
	{
		ft_putstr("lst[");
		ft_putstr(ft_itoa(i));
		ft_putstr("] : ");
		if (ft_isreadable(lst->content, lst->content_size))
			ft_putstr(lst->content);
		else
			ft_putnbr(*(int *)lst->content);
		ft_putstr("   /* size = ");
		ft_putnbr(lst->content_size);
		ft_putchar('\n');
		lst = lst->next;
		i++;
	}
}
