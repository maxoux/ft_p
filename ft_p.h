#ifndef FT_P_H
# define FT_P_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include "libft/libft.h"

#include <stdio.h>
#include <errno.h>

#define CMD_LS 1
#define CMD_PWD 2
#define CMD_CD 3
#define CMD_GET 4
#define CMD_PUT 5
#define CMD_QUIT 9

typedef struct		s_msgheader {
	char			cmd;
	char			name[256];
	long long int	part;
	long long int	nb_part;
	long long int	size_file;
	long long int	payload;
}					t_msgheader;

typedef struct		s_response {
	char			cmd;
	char			status;
	char			msg[1024];
}					t_response;

#endif