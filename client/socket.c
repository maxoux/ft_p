#include "client.h"

int init_socket(char *host, int port)
{
	int 				socket_fd;
	struct sockaddr_in	params;
	int					state;

	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1)
		return (-1);
	params.sin_addr.s_addr = inet_addr(host);
	params.sin_family = AF_INET;
	params.sin_port = htons(port);
	state = connect(socket_fd, (struct sockaddr*)&params, sizeof(params));
	if (state == -1)
		return (-1);
	else
		return (socket_fd);
}