#include "client.h"

void test_ls(int sock)
{
	t_msgheader msg;
	int			sended;

	msg.cmd = CMD_LS;
	sended = send(sock, &msg, sizeof(msg), 0);
	if (sended == -1)
	{
		printf("erreur : %s\n", strerror(errno));
	}
	else
		printf("sended !\n");
}

int main(int argc, char **argv)
{
	int	sock;

	if (argc < 3 || !ft_isnbr(argv[2]))
	{
		ft_putendl("Usage: \n./server <host> <port>\n");
		return (0);
	}
	sock = init_socket(argv[1], ft_atoi(argv[2]));
	if (sock == -1)
	{
		ft_putendl("Connection failed !");
		return (0);
	}

	test_ls(sock);

	return (0);
}