#include "server.h"



int main(int argc, char **argv)
{
	int sock;

	if (argc < 2 || ft_isnbr(argv[1]) == 0)
	{
		ft_putendl("Usage: \n./client <port>\n");
		return (0);
	}
	sock = init_socket_listener(ft_atoi(argv[1]));
	if (sock == -1)
	{
		ft_putendl("error on opening socket");
		return (0);
	}
	listen_server(sock);
	return (0);
}