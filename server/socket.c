#include "server.h"

int init_socket_listener(int port)
{
	int socket_fd;
	struct sockaddr_in	params;

	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1)
		return (-1);
	params.sin_addr.s_addr = htonl(INADDR_ANY);
	params.sin_family = AF_INET;
	params.sin_port = htons(port);
	if (bind(socket_fd, (struct sockaddr*)&params, sizeof(params)) == -1)
		return (-1);
	else if (listen(socket_fd, MAX_USERS) == -1)
		return (-1);
	else
	{
		ft_putstr("server listening on port ");
		ft_putnbr(port);
		ft_putendl("");
		return (socket_fd);
	}
}