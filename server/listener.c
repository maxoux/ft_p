#include "server.h"

t_exec_cmd cmd[] = {
	{CMD_LS, noop},
	{CMD_PWD, noop},
	{CMD_CD, noop},
	{CMD_GET, noop},
	{CMD_PUT, noop},
	{CMD_QUIT, noop},
	{-1, noop}
};

void	noop(t_msgheader msg)
{
	printf("msg received ! request : %d\n", msg.cmd);
	(void)msg;
}

static void	fork_listeners(void)
{
	int	child;
	int	pid;

	child = 1;
	pid = 1;
	while(pid && child < MAX_USERS)
	{
		pid = fork();
		child++;
	}

}

static void	receive_cmd(int csock)
{
	t_msgheader	msg;
	char		flag;
	int			readed;
	int			i;

	flag = 1;
	while (flag)
	{
		readed = recv(csock, &msg, sizeof(msg), 0);
		i = 0;
		while (cmd[i].type != -1)
		{
			if (cmd[i].type == msg.cmd)
				cmd[i].f(msg);
			i++;
		}
		ft_bzero(&msg, sizeof(msg));
		if (msg.cmd == CMD_QUIT || readed != sizeof(msg))
			flag = 0;
	}
	shutdown(csock, 2);
}

int	listen_server(int sock)
{
	struct sockaddr_in	csin;
	socklen_t			size;
	int					csock;

	fork_listeners();
	size = sizeof(csin);
	csock = accept(sock, (struct sockaddr*)&csin, &size);
	receive_cmd(csock);
	return (1);
}