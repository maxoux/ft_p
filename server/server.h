#ifndef SERVER_H
# define SERVER_H

// To delete
#include <stdio.h>

#include "../ft_p.h"

#define MAX_USERS 12

typedef struct		s_user {
	char			pwd[1024];

}					t_user;

typedef struct		s_exec_cmd {
	char			type;
	void			(*f)(t_msgheader msg);
}					t_exec_cmd;

int 	init_socket_listener(int port);
int		listen_server(int sock);
void	noop(t_msgheader msg);

#endif